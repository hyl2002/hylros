# hylROS

这个一个关于ROs学习的仓库
ros study road

### 10月15号

观看视频P1—P5

​	前五章视频讲了什么是ros，ros的相关概念是什么，ros的发展例程等等，ros的诞生背景看完之后我也有了相关的了解和认知，知道了问什么人们迫切希望拥有一个适用于机器人的通用的软件框架，ros是一个开源的元操作系统，里面可以集成大量的工具和库，以及协议，有人把它可以被看作是通讯+工具+功能+生态，关于通讯方面，今天我有空的时候也了解了一些相关的内容，比如就是关于rostopic的方面,每个节点会接受自己感兴趣的话题，以及关于交互式通讯所采用的Service，有了初步的了解，虽然说目前还不是很懂，但是也大概清楚了，对于通讯方面，我们要采用的是Service（一个发送ROS请求，一个来接受请求），然后我比较好奇的查阅了ros系统的设计框架，ros有一个工作区，我们将在工作空间内对空间配置一些环境变量，随后依据自己的编程习惯，安装功能包，配置xml文件。

​	目前对ros元操作系统的框架有了初步的了解。![11](C:\Users\HYL\Desktop\11.png)

### 10月17号

观看视频p6-p38页

由于之前已经在自己的windows上下载过虚拟机VWware,所以对于视频中的一些操作，有的直接过滤掉了，在自己的虚拟机中下载了Ubuntu18.04,对于自己的虚拟机的优化问题（大概就是）自己的电脑无法与虚拟机内的系统进行文件交互，所以自己将VMware中的tools删除，重新install了open-vm-tools工具，最后成功完成互传工作。

**使用代码 **

>1. `sudo vmware-uninstall-tools.pl`
>2. 有图形化的使用
>3. `sudo apt-get open-vm-tools-desktop`

**设置安装源**

这里采用了清华源

>sudo sh -c '. /etc/lsb-release && echo "deb http://mirrors.tuna.tsinghua.edu.cn/ros/ubuntu/ `lsb_release -cs` main" > /etc/apt/sources.list.d/ros-latest.list'

**设置key**

> sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

**更新apt使这个工具处于最新的版本**

> sudo apt update

**由于电脑的ubuntu系统为18.04的所以找到ros对应的版本是melodic**

>sudo apt install ros-moetic-desktop-full

**配置环境变量，方便任意使用**

> echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
> source ~/.bashrc

**安装python3**

**下载resdep**

在安装好ros系统时候，对ros进行测试，从虚拟机的ubuntu系统的软件安装中下载终结终端，此软件是为了**实现可以打卡多个终端的功能**，分别输入rescore，来打开ros核心，后打开乌龟节点

语法为rosrun+功能包的名称+节点的名称

> rosrun turtlesim turtlesim_node
>
> rosrun turlesim turtle_teleop_key

值得注意的是必须要将光标放在读取命令的那一个窗口中，使得键盘的输入命令输入到该窗口能被接收到，进而可以让乌龟运动起来

### 写一个简单的c++程序

**步骤1：先创建一个工作空间**

>mkdir -p 自定义空间名称/src
>cd 自定义空间名称
>catkin_make

其中catkin_make的功能是编译并生成一个可以执行的工程

**步骤2：再创造一个功能包**

> 进入到src，创建功能包并且添加依赖，支持的语言+消息标准库
>
> cd src
> catkin_create_pkg 自定义ROS包名 roscpp rospy std_msgs

**步骤3：编辑源文件**

进入到自定义的ros包里面然后打开src，编写源文件，这里我用的是c++编写的源文件

**步骤4：编辑配置文件**

编写完源文件后要对自定义的包下的Cmakelist.txt文本进行编辑，将可执行文件映射到一个名字，然后使用tagert_link_libraries进行链接库，注意这里使用的还是之前你映射的文件名字

> add_executable(步骤3的源文件名
>   src/步骤3的源文件名.cpp
> )
> target_link_libraries(步骤3的源文件名
>   ${catkin_LIBRARIES}
> )

**步骤5:编译并执行**

> catkin_make

**launch文件的使用**

 对于官方给出的多节点同时启动的优化策略，使用launch文件进行多节点启动，在功能包的子目录下创建launch文件，编写launch文件的内容，launch的语法是：pkg=功能包的名字，type是节点的名称，name是自己的随便命名的东西，如果想让日志输出那就加上另一个属性，output="screen"

><launch>
>    <node pkg="helloworld" type="demo_hello" name="hello" output="screen" />
>    <node pkg="turtlesim" type="turtlesim_node" name="t1"/>
>    <node pkg="turtlesim" type="turtle_teleop_key" name="key1" />
></launch>

![1](README.assets/1-1666196578315.png)

最终达到的目的就是实现了多个节点的同时运行

还有一个比较重要的是显示各个节点的关系，下载安装rqt_graph

>$ sudo apt install ros-melodic-rqt
>$ sudo apt install ros-melodic-rqt-common-plugins

在这里由于我用的是ubuntu18.04,对应的应该是melodic,下载好rqt_graph,打开终端想使用的时候直接输入rqt_grapth就可以显示图

![2](README.assets/2-1666196593128.png)

<center>**第一章学习完毕**
---
### 

##                                           第二章学习

## 11月14号第四章节学习











## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/hyl2002/hylros.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/hyl2002/hylros/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
